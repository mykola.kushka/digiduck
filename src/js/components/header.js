'use strict';

export function headerSticky() {
  window.onscroll = function () {
    headerPosition();
  };

  const headerMain = document.querySelector('#headerMain');

  const headerPosition = () => {
    if (window.pageYOffset > 0) {
      headerMain.classList.add('header-main-sticky');
    } else {
      headerMain.classList.remove('header-main-sticky');
    }
  };

  headerPosition();
}
